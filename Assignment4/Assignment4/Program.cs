using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

/// <summary>
/// This assignment include 3 methods to create normal distribution and one way to create pairs of joint normally distributed random values
/// </summary>
namespace Assignment4
{
    class Program
    {
        static Random rnd = new Random();// To create a global value of random value to make each one is unique.
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            double[] NormalArr_TU = new double[1000];// Create a array of three methods to save the value of random number
            double[] NormalArr_BM = new double[1000];
            double[] NormalArr_PR = new double[1000];
            double[,] Arr_JointNormal = new double[1000,2];// Pairs of joint normally distributed random values
            for (int i = 0; i < 1000; i++)// Get the normal random value of three methods
            {
                NormalArr_TU[i] = TwelveUniform();
                NormalArr_BM[i] = BoxMuller();
                NormalArr_PR[i] = PolarRejection();                
            }

            Console.WriteLine("Now we test whether they are standard normal distribution\n");
            // Get the expectation of three methods
            double teste_TU = GetE(NormalArr_TU);
            double teste_BM = GetE(NormalArr_BM);
            double teste_PR = GetE(NormalArr_PR);
            // Get the variation of three methods
            double testd_TU = GetD(NormalArr_TU, teste_TU);
            double testd_BM = GetD(NormalArr_BM, teste_BM);
            double testd_PR = GetD(NormalArr_PR, teste_PR);
            // Get the Skewness of three methods
            double testS_TU = GetS(NormalArr_TU, teste_TU, testd_TU);
            double testS_BM = GetS(NormalArr_BM, teste_BM, testd_BM);
            double testS_PR = GetS(NormalArr_PR, teste_PR, testd_PR);
            // Get the Kurtosis of three methods
            double testK_TU = GetK(NormalArr_TU, teste_TU, testd_TU);
            double testK_BM = GetK(NormalArr_BM, teste_BM, testd_BM);
            double testK_PR = GetK(NormalArr_PR, teste_PR, testd_PR);
            // Output the 4 kinds feature value of three methods
            Console.WriteLine(string.Format("TwelveUniform's Expectation is:{0}, " +
                "Variation is:{1}, \n \t Skewness is: {2}, Kurtosis is: {3}", teste_TU, testd_TU, testS_TU, testK_TU));
            Console.WriteLine(string.Format("BoxMuller's Expectation is:{0} " +
                "Variation is:{1}, \n \t Skewness is: {2}, Kurtosis is: {3}", teste_BM, testd_BM, testS_BM, testK_BM));
            Console.WriteLine(string.Format("PolarRejection's Expectation is:{0} " +
                "Variation is:{1}, \n \t Skewness is: {2}, Kurtosis is: {3}", teste_PR, testd_PR, testS_PR, testK_PR));
            // Draw the picture of three methods' sample with "#"
            Console.WriteLine("The picture of TwelveUniform method:");
            Draw(NormalArr_TU);
            Console.Write("\n");
            Console.WriteLine("The picture of BoxMuller method:");
            Draw(NormalArr_BM);
            Console.Write("\n");
            Console.WriteLine("The picture of PolarRejection method:");
            Draw(NormalArr_PR);
            Console.Write("\n");
            
            Console.WriteLine("How many pairs of joint normally distributed random values");
            int n = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("What the correlation you want please choose from -1 to 1:");
            double rho = Convert.ToDouble(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Arr_JointNormal[i,j] = JointNormally(rho)[j];
                }
                Console.WriteLine(string.Format("The first one is:{0}, \t the second one is:{1}", Arr_JointNormal[i,0], Arr_JointNormal[i, 1]));
            }            
            Console.ReadLine();
            
        }
        /// <summary>
        /// Twelve Uniform Methods to create normal distribution
        /// </summary>
        /// <returns></returns>
        static double TwelveUniform()
        {
            //Random rnd = new Random();//unchecked((int)DateTime.Now.Ticks)); //reuse this if you are generating many
            double u1 = 0;
            for (int i = 1; i <= 12; i++)// Add 12 uniform distribution with the mean 6 and variation 1
            {
                double u2 = rnd.NextDouble();
                u1 += u2;
                //Thread.Sleep(25);
            }
            return u1-6;// Create a standard nromal distribution
        }
        /// <summary>
        /// Box Muller Methods
        /// </summary>
        /// <returns></returns>
        static double BoxMuller()
        {
            //Random rnd = new Random(unchecked((int)DateTime.Now.Ticks)); //reuse this if you are generating many
            double u1 = rnd.NextDouble();
            //Thread.Sleep(25);
            double u2 = rnd.NextDouble();
            return Math.Sqrt(-2 * Math.Log(u1)) * Math.Cos(2 * Math.PI * u2);// Create a standard nromal distribution
        }
        /// <summary>
        /// PolarRejection Methods
        /// </summary>
        /// <returns></returns>
        static double PolarRejection()
        {
            //Random rnd = new Random(unchecked((int)DateTime.Now.Ticks)); //reuse this if you are generating many
            double u1 = 2 * rnd.NextDouble() - 1;
            //Thread.Sleep(25);
            double u2 = 2 * rnd.NextDouble() - 1;
            double c;
            while(Math.Pow(u1, 2) + Math.Pow(u2, 2) > 1)
            {
                u1 = 2 * rnd.NextDouble() - 1;
                //Thread.Sleep(25);
                u2 = 2 * rnd.NextDouble() - 1;
            }
            c = Math.Sqrt(-2 * Math.Log(Math.Pow(u1, 2) + Math.Pow(u2, 2)) / (Math.Pow(u1, 2) + Math.Pow(u2, 2)));
            return c * u1;// Create a standard nromal distribution
        }

        /// <summary>
        /// Create one pair of joint normally distributed random values
        /// </summary>
        /// <param name="rho"> The correlation we need to input </param>
        /// <returns> A array of one pair of joint normally distributed random values</returns>
        static double[] JointNormally(double rho)
        {
            double u1 = 2 * rnd.NextDouble() - 1;
            //Thread.Sleep(25);
            double u2 = 2 * rnd.NextDouble() - 1;
            double c, n1, n2, n3;
            double[] JointNormal = new double[2];
            while (Math.Pow(u1, 2) + Math.Pow(u2, 2) > 1)
            {
                u1 = 2 * rnd.NextDouble() - 1;
                //Thread.Sleep(25);
                u2 = 2 * rnd.NextDouble() - 1;
            }
            c = Math.Sqrt(-2 * Math.Log(Math.Pow(u1, 2) + Math.Pow(u2, 2)) / (Math.Pow(u1, 2) + Math.Pow(u2, 2)));
            n1 = c * u1;// Create a standard nromal distribution
            n2 = c * u2;
            n3 = rho * n1 + Math.Sqrt(1 - Math.Pow(rho, 2)) * n2;// Easy way to create a joint normally distributed random value with n1
            JointNormal[0] = n1;
            JointNormal[1] = n3;
            return JointNormal;
        }
        /// <summary>
        /// Caculate the expectation of sample
        /// </summary>
        /// <param name="arr">The array of the sample set</param>
        /// <returns>teste is the expectation of sample </returns>
        static double GetE(double[] arr)
        {
            double teste;
            double sumresult = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                sumresult += arr[i];
            }
            teste = sumresult / arr.Length;
            return teste;
        }

        /// <summary>
        /// Caculate the variation of sample
        /// </summary>
        /// <param name="arr"> The array of the sample set</param>
        /// <param name="teste"> The expectation of sample</param>
        /// <returns> The testd is the variation </returns>
        static double GetD(double[] arr, double teste)
        {
            double sumd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sumd += Math.Pow(arr[i] - teste, 2);
            }
            return sumd / arr.Length;
        }

        /// <summary>
        /// Caculate the Skewness of sample
        /// </summary>
        /// <param name="arr"> The array of the sample set </param>
        /// <param name="teste"> The expectation of sample </param>
        /// <param name="testd"> The variation of sample</param>
        /// <returns> The value of Skewness </returns>
        static double GetS(double[] arr, double teste, double testd)
        {
            double sumd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sumd += Math.Pow(arr[i] - teste, 3);
            }
            return sumd / arr.Length / Math.Pow(Math.Sqrt(testd), 3);
        }

        /// <summary>
        /// Caculate the Kurtosis of sample
        /// </summary>
        /// <param name="arr"> The array of the sample set </param>
        /// <param name="teste"> The expectation of sample </param>
        /// <param name="testd"> The variation of sample</param>
        /// <returns> The value of Kurtosis </returns>
        static double GetK(double[] arr, double teste, double testd)
        {
            double sumd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sumd += Math.Pow(arr[i] - teste, 4);
            }
            return sumd / arr.Length / Math.Pow(testd, 2);
        }

        /// <summary>
        /// This methods is try to draw distribution picture of sample
        /// </summary>
        /// <param name="arr"> The array of the sample set </param>
        /// <returns> The number of each interval </returns>
        static double[] Draw(double[] arr)
        {
            string singal = "#";
            double[] number = new double[10];
            number[0] = 0;
            number[1] = 0;
            number[2] = 0;
            number[3] = 0;
            number[4] = 0;
            number[5] = 0;
            number[6] = 0;
            number[7] = 0;
            number[8] = 0;
            number[9] = 0;
            for (int i = 0; i < arr.Length; i++)// Statistic the number in each intervals:
            {
                if(arr[i] <= 0.5 && arr[i] > 0)//(0,0.5]
                {
                    number[5] += 1;
                }
                else if (arr[i] > -0.5 && arr[i] <= 0)//(-0.5,0]
                {
                    number[4] += 1;
                }
                else if(arr[i] + 1 > 0 && arr[i] + 0.5 <= 0)//(-1,-0.5]
                {
                    number[3] += 1;
                }
                else if (arr[i] - 1 <= 0 && arr[i] - 0.5 > 0)//(0.5,1]
                {
                    number[6] += 1;
                }
                else if (arr[i] + 1.5 > 0 && arr[i] + 1 <= 0)//(-1.5,-1]
                {
                    number[2] += 1;
                }
                else if (arr[i] - 1.5 <= 0 && arr[i] - 1 > 0)//(1,1.5]
                {
                    number[7] += 1;
                }
                else if (arr[i] + 2 > 0 && arr[i] + 1.5 <= 0)//(-2,-1.5]
                {
                    number[1] += 1;
                }
                else if (arr[i] - 2 <= 0 && arr[i] - 1.5 > 0)//(1.5,2]
                {
                    number[8] += 1;
                }
                else if (arr[i] + 2 <= 0)//(-inf,-2]
                {
                    number[0] += 1;
                }
                else if (arr[i] - 2 > 0)//(2,inf)
                {
                    number[9] += 1;
                }
            }

            for (int i = 0; i < number.Length / 2; i++)// Draw the picture with "#" from left side
            {
                
                //Console.Write("\t");
                for (int j = 0; j < number[i] / 10; j++)// If the sample is too large, reduce the sample size
                {
                    Console.Write(singal);                    
                }
                Console.Write("\n");
                Console.Write(0.5 * i - 2);// Output the Demarcation point
                Console.Write("\n");
            }
            for (int i = 5; i < number.Length; i++)// Draw the picture with "#" from right side
            {

                //Console.Write("\t");
                for (int j = 0; j < number[i] / 10; j++)// If the sample is too large, reduce the sample size
                {
                    Console.Write(singal);
                }
                if(i != 9)
                {
                    Console.Write("\n");
                    Console.Write(0.5 * i - 2);// Output the Demarcation point
                    Console.Write("\n");
                }
            }
            Console.Write("\n");
            return number;
        }
    }
}
